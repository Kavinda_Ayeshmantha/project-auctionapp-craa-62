package com.kongcepts.crud.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudKongceptsFinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudKongceptsFinalApplication.class, args);
	}

}
