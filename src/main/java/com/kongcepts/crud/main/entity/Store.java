package com.kongcepts.crud.main.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;



@Entity
@Data
@Table(name = "tbl_store")
public class Store {

    @Id
    @GeneratedValue
    @Column(name="tbl_store_id",unique = true, nullable = false)
    private int id;
    
    @Column(name="tbl_store_name")
    private String name;
    
    @OneToMany(mappedBy = "store", cascade = CascadeType.ALL)
    private List<StoreConfig> config;
    

	
}
