package com.kongcepts.crud.main.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;



@Entity
@Data
@Table(name = "tbl_store_config")
public class StoreConfig {

	
	//There is no filed name 
    @Id
    @GeneratedValue
    @Column(name = "tbl_store_config_id", unique = true, nullable = false)
    private int id;
    @Column(name="tbl_store_config_base_color", nullable = true)
 	private String baseColor ;
    @Column(name="tbl_store_config_font_color", nullable = true)
    private String fontColor ;
    @Column(name="tbl_store_config_logo", nullable = true)
    private String logo ;
    @Column(name="tbl_store_config_isenable", nullable = true)
    private int isEnable ;
    @Column(name="tbl_store_config_domain", nullable = true)
    private String domain ; 
    @Column(name="tbl_store_config_email", nullable = true)
    private String email ;
    @Column(name="tbl_store_config_contact", nullable = true)
    private String contact;
    
    @JsonIgnore
    @ManyToOne()
    @JoinColumn(name = "fk_store_id", referencedColumnName = "tbl_store_id")

    Store store;

	
}
