package com.kongcepts.crud.main.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kongcepts.crud.main.entity.StoreConfig;

public interface StoreConfigRepository extends JpaRepository<StoreConfig, Integer> {

	List<StoreConfig> findByEmail(String email);

}
