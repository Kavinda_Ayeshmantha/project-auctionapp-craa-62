package com.kongcepts.crud.main.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kongcepts.crud.main.entity.Store;



@Repository
public interface StoreRepository extends JpaRepository<Store, Integer> {
	
	List<Store> findByName(String name);
	
}
