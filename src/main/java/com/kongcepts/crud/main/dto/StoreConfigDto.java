package com.kongcepts.crud.main.dto;

import com.kongcepts.crud.main.entity.StoreConfig;

import lombok.Data;

@Data
public class StoreConfigDto {

	// If you want the store_config section to show the same store_id, then 
	// this will be the store_id.
    private int id;    
    
 	private String baseColor ;
    private String fontColor ;
    private String logo ;
    private int isEnable ;
    private String domain ; 
    private String email ;
    private String contact;
	
    public StoreConfigDto(StoreConfig sc) {
    	this.id = sc.getStore().getId();
    	this.baseColor = sc.getBaseColor();
    	this.fontColor = sc.getFontColor();
    	this.logo = sc.getLogo();
    	this.isEnable = sc.getIsEnable();
    	this.domain = sc.getDomain();
    	this.email = sc.getEmail();
    	this.contact = sc.getContact();
    }
}
