package com.kongcepts.crud.main.dto;

import java.util.List;

import com.kongcepts.crud.main.entity.Store;

import lombok.Data;

@Data
public class StoreDto {
    private int id;
    
    private String name;  

    private List<StoreConfigDto> config;
    
    public StoreDto(Store store) {
    	this.id = store.getId();
    	this.name = store.getName();
    }
    
}
