package com.kongcepts.crud.main.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kongcepts.crud.main.entity.StoreConfig;
import com.kongcepts.crud.main.service.StoreConfigService;

@RestController
public class StoreConfigController {

	@Autowired
	private StoreConfigService service;

	// Adding
	@PostMapping("/adding")
	public StoreConfig addStoreConfig(@RequestBody StoreConfig storeconfig) {
		return service.saveStoreConfig(storeconfig);
	}

	// Get by Id
	@GetMapping("/storeconfig/{id}")
	public StoreConfig findStoreConfigById(@PathVariable int id) {
		return service.getStoreConfigById(id);
	}

	// Get by Email
	@GetMapping("/storeconfig/email/{email}")
	public StoreConfig findStoreConfigByEmail(@PathVariable String email) {
		return service.getStoreConfigByEmail(email);
	}

	// Delete by Id
	@DeleteMapping("/delete/id/{id}")
	public String deleteStoreConfig(@PathVariable int id) {
		return service.deleteStoreConfig(id);
	}

	//view all data
	@GetMapping("/getallstore")
	public List<StoreConfig> findAllStoreConfigs() {
		return service.getStoreConfigs();
	}

}
