package com.kongcepts.crud.main.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kongcepts.crud.main.dto.StoreDto;
import com.kongcepts.crud.main.entity.Store;
import com.kongcepts.crud.main.service.StoreService;


@RestController
public class StoreController {

    @Autowired
    private StoreService service;
    
    
    //Adding new 
    @PostMapping("/add")
    public StoreDto addStore(@RequestBody Store store){
        return service.saveStore(store);
    }
    //adding more at once
    @PostMapping("/addmore")
    public List<Store> addStore(@RequestBody List<Store> stores){
        return service.saveStores(stores);
    }
    //View with Id
    @GetMapping("/getstore/id/{tbl_store_id}")
    public Store findStoreById(@PathVariable int tbl_store_id){
        return service.getStoreById(tbl_store_id);
    }
    //View with name
    @GetMapping("/getstore/name/{tbl_store_name}")
    public Store findStoreByName(@PathVariable String tbl_store_name){
        return service.getStoreByName(tbl_store_name);
    }
    //Updating
    @PutMapping("/update")
    public Store updateStore(@RequestBody Store store) {
        return service.updateStore(store);
    }
    //Deleting
    @DeleteMapping("/delete/{storeId}")
    public String deleteStore(@PathVariable int storeId){
        return service.deleteStore(storeId);
    }
 // View all data on the DB
    @GetMapping("/getstore")
    public List<Store> findAllStores(){
        return service.getStores();
    }
}
