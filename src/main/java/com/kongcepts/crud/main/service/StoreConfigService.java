package com.kongcepts.crud.main.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kongcepts.crud.main.entity.Store;
import com.kongcepts.crud.main.entity.StoreConfig;
import com.kongcepts.crud.main.repository.StoreConfigRepository;

@Service
public class StoreConfigService {

	@Autowired
	private StoreConfigRepository repository;

	// Add one
	public StoreConfig saveStoreConfig(StoreConfig storeconfig) {
		return repository.save(storeconfig);
	}

	// Find by Id
	public StoreConfig getStoreConfigById(int id) {
		return repository.findById(id).orElse(null);
	}

	// Find by Email
	public StoreConfig getStoreConfigByEmail(String email) {
		List<StoreConfig> storeconfigs = repository.findByEmail(email);
		return storeconfigs != null && !storeconfigs.isEmpty() ? storeconfigs.get(0) : null;
	}

	// Deleting
	public String deleteStoreConfig(int id) {
		repository.deleteById(id);
		return "Succesfully Deleted.." + id;
	}

	// shows all available in DB
	public List<StoreConfig> getStoreConfigs() {
		return repository.findAll();
	}
}
