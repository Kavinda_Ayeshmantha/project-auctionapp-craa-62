package com.kongcepts.crud.main.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kongcepts.crud.main.dto.StoreConfigDto;
import com.kongcepts.crud.main.dto.StoreDto;
import com.kongcepts.crud.main.entity.Store;
import com.kongcepts.crud.main.entity.StoreConfig;
import com.kongcepts.crud.main.repository.StoreRepository;

@Service
@Transactional
public class StoreService {
	
		@Autowired
	    private StoreRepository repository;

	    // Saving single object
	    public StoreDto saveStore(Store store){
	    	Store newStore = new Store();
	    	newStore.setName(store.getName());

	    	List<StoreConfig> configs = new ArrayList<>();
	    	for (StoreConfig sc : store.getConfig()) {
	    		// Associate the store and the store config.
	    		sc.setStore(newStore);
	    		configs.add(sc);
	    	}
	    	
	    	newStore.setConfig(configs);
	    	
	       Store savedStore = repository.save(newStore);
	       
	       StoreDto storeDto = new StoreDto(savedStore);
	       List<StoreConfigDto> configDtos = new ArrayList<>();
	       for (StoreConfig sc : configs) {
	    	   StoreConfigDto dto = new StoreConfigDto(sc);
	    	   configDtos.add(dto);
	       }
	       
	       storeDto.setConfig(configDtos);
	       
	       return storeDto;
	       
	    }
	    // saving a list of objects
	    public List<Store> saveStores(List<Store> stores){
	        return repository.saveAll(stores);
	    }

	    // shows all available in DB
	    public List<Store> getStores(){
	        return repository.findAll();
	    }

	    // Finding by Id
	    public Store getStoreById (int tbl_store_id){
	        return repository.findById(tbl_store_id).orElse(null);
	    }

	    //FINDING BY NAME
	    public Store getStoreByName (String name){
	        List<Store> stores = repository.findByName(name);
	        return stores != null && !stores.isEmpty() ? stores.get(0) : null;
	    }

	    //Deleting Method
	    public String deleteStore(int tbl_store_id){
	        repository.deleteById(tbl_store_id);
	        return "Successfully Deleted..." +tbl_store_id;
	    }

	    //Updating Method
	    public Store updateStore(Store store){
	        Store existitingStore=repository.findById(store.getId()).orElse(null);
	        existitingStore.setName(store.getName());
	        return repository.save(existitingStore);

	    }
	}
